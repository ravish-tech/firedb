import log from "loglevel";
import actions from "./actions";
import mutations from "./mutations";

afterEach(() => { jest.clearAllMocks(); });

jest.mock("loglevel");
jest.mock("firebase/app");

function getCommit(state) {
    return jest.fn((type, payload) => {
        // if (type !== "setBusy") console.log("Calling:", type, payload);
        mutations[type](state, payload);
        // if (type !== "setBusy") console.log("State:", JSON.stringify(state));
    });
}

describe("Testing actions", () => {

    test("To register and unregister collection successfully", async () => {
        const state = { isBusy: false, _registeredPaths: {} };
        const path = "c1/d1/c2";
        await actions.registerPath({ state, commit: getCommit(state) } as any, path);
        expect(state._registeredPaths).toHaveProperty(path);
        expect(state).toHaveProperty("c1.d1.c2.doc1");
        expect(state).not.toHaveProperty("c1.d1.c2.doc2");
        await actions.unregisterPath({ state, commit: getCommit(state) } as any, path);
        expect(state._registeredPaths).not.toHaveProperty(path);
        expect(state).not.toHaveProperty("c1.d1.c2.doc1");
    });

    test("To register and unregister documents successfully", async () => {
        const state = { isBusy: false, _registeredPaths: {} };
        const path = "c1/doc1";
        await actions.registerPath({ state, commit: getCommit(state) } as any, path);
        expect(state._registeredPaths).toHaveProperty(path);
        expect(state).toHaveProperty("c1.doc1");

        await actions.unregisterPath({ state, commit: getCommit(state) } as any, path);
        expect(state._registeredPaths).not.toHaveProperty(path);
        expect(state).not.toHaveProperty("c1.doc1");
    });

    test("to handle blank path", async () => {
        const state = { isBusy: false, _registeredPaths: { "c1/d1/c2": true } };
        const state2 = JSON.parse(JSON.stringify(state));
        const path = "";
        await actions.registerPath({ state, commit: getCommit(state) } as any, path);
        expect(log.error).toHaveBeenCalled();
        expect(state).toEqual(state2);
    });

    test("to throw error when registering/unregistering invalid paths.", async () => {
        const state = { isBusy: false, _registeredPaths: { "c1/d1/c2": true } };
        const state2 = JSON.parse(JSON.stringify(state));
        let path = "c1/d1/c2";
        await actions.registerPath({ state, commit: getCommit(state) } as any, path);
        expect(log.error).toHaveBeenCalled();
        expect(state).toEqual(state2);
        jest.clearAllMocks();
        path = "c1/d1/invalid";
        await actions.unregisterPath({ state, commit: getCommit(state) } as any, path);
        expect(log.error).toHaveBeenCalled();
        expect(state).toEqual(state2);
    });

    test("to use set to add document", async () => {
        const state = { isBusy: false, _registeredPaths: {} };
        const path = "c1/d1/c2";
        await actions.registerPath({ state, commit: getCommit(state) } as any, path);
        await actions.set({ state, commit: getCommit(state) } as any, {
            path: path, value: { a: 1 }
        });
        expect(state).toHaveProperty("c1.d1.c2.newref.a");
    });

    test("to use set to create new document with provided docref", async () => {
        const state = { isBusy: false, _registeredPaths: {} };
        const path = "c1";
        await actions.registerPath({ state, commit: getCommit(state) } as any, path);
        await actions.set({ state, commit: getCommit(state) } as any, {
            path: path + "/d1", value: { a: 1 }
        });
        expect(state).toHaveProperty("c1.d1.a");
    });

    test("to use set to update document", async () => {
        const state = { isBusy: false, _registeredPaths: {} };
        const path = "c1/d1/c2";
        await actions.registerPath({ state, commit: getCommit(state) } as any, path);
        await actions.set({ state, commit: getCommit(state) } as any, {
            path: path + "/doc1", value: { aa: 1 }
        });
        expect(state).toHaveProperty("c1.d1.c2.doc1.aa");
    });

    test("to use set to remove document", async () => {
        const state = { isBusy: false, _registeredPaths: {} };
        const path = "c1/d1/c2";
        await actions.registerPath({ state, commit: getCommit(state) } as any, path);
        // Trying to remove doc1 which is added while registering path
        await actions.set({ state, commit: getCommit(state) } as any, {
            path: path + "/doc1", value: null
        });
        expect(state).not.toHaveProperty("c1.d1.c2.doc1");
    });

    test("to use set unregistered path and expect warning", async () => {
        const state = { isBusy: false, _registeredPaths: {} };
        const path = "c1/d1/c2";
        await actions.set({ state, commit: getCommit(state) } as any, {
            path: path, value: { a: 1 }
        });
        expect(log.warn).toHaveBeenCalled();
    });

    test("to handle queries", async () => {
        const state = { isBusy: false, _registeredPaths: {} };
        const path = "c1?a=b";
        await actions.registerPath({ state, commit: getCommit(state) } as any, path);
        expect(state._registeredPaths).toHaveProperty("c1");
        expect(state).toHaveProperty("c1.doc1");
        expect(state).not.toHaveProperty("c1.doc2");
    });

});
