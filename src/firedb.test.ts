import firedb from "./firedb";

jest.mock("loglevel");

const registerModule = jest.fn();

describe("Testing firedb", () => {
    test("to return a Vuex plugin", () => {
        firedb({ registerModule } as any);
        expect(registerModule).toBeCalledWith(["firedb"], expect.anything());
    })
});