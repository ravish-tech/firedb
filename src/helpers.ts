/**
 * Returns elements of a firestore path as string array.
 * @param path Path to collection or a document
 */
export function getPathRefs(path: string) {
  return path ? path.trim().split("/").filter(r => !!r) : [];
}

/**
 * Returns collection-path and doc-reference from path.
 * If path points to collection, docRef will be null.
 * @param path Path to a collection or document
 */
export function getNormalisedPathAndDocRef(path: string) {
  const keys = getPathRefs(path);
  const docRef = keys.length % 2 === 0 ? keys.pop() : null;
  return { collectionPath: keys.join("/"), docRef: docRef || null };
}

/**
 * Extracts queries from the path and return path only and queries as Array[<string>,<string>,<string>]
 * @param path full path with optional query string
 */
export function extractQueries(path: string) {
  const pathParts = path ? path.split("?") : [""];
  const result = { pathWithoutQuery: pathParts[0], queries: [] }
  if (pathParts.length > 1) {
    const queries = pathParts[1].split("&");
    queries.forEach((q) => {
      if (q.trim()) {
        const a = q.match(/^([a-zA-Z-]+)(<|<=|==|>=|>|array-contains)([a-zA-Z0-9-]+)$/i);
        if (a && a[1] && a[2] && a[3]) {
          result.queries.push([a[1], a[2], a[3]]);
        }
      }
    });
  }
  return result;
}