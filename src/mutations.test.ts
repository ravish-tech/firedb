import mutations from "./mutations";
import log from "loglevel";

jest.mock("loglevel");
afterEach(() => { jest.clearAllMocks(); });

describe("Testing Mutations", () => {

    //setDoc
    test("setDoc: sets a document on existing collection", () => {
        const state: any = { _registeredPaths: {}, c1: { d1: { c11: {} } } };
        mutations.setDoc(state, { documentPath: "c1/d1/c11/d11", value: { a: 1 } });
        expect(state.c1.d1.c11.d11.a).toEqual(1);
    });
    test("setDoc: sets a document on non-existing collection", () => {
        const state: any = { _registeredPaths: {}, };
        mutations.setDoc(state, { documentPath: "c1/d1/c11/d11", value: { a: 1 } });
        expect(state.c1.d1.c11.d11.a).toEqual(1);
    });
    test("setDoc: removes a exisiting doc", () => {
        const state: any = { _registeredPaths: {}, c1: { d1: { c2: { d1: {}, d2: {} } } } };
        mutations.setDoc(state, { documentPath: "c1/d1/c2/d2" });
        expect(state.c1.d1.c2.d1).not.toBeUndefined();
        expect(state.c1.d1.c2.d2).toBeUndefined();
    });
    test("setDoc: with invalid input", () => {
        const state: any = { _registeredPaths: {}, c1: { d1: { c11: {} } } };
        const state2 = JSON.parse(JSON.stringify(state));
        // path must be to a document
        mutations.setDoc(state, { documentPath: "invalid", value: { a: 1 } });
        mutations.setDoc(state, { documentPath: "a/invalid/path", value: { a: 1 } });
        // path cannot be empty
        mutations.setDoc(state, { documentPath: "", value: { a: 1 } });
        // Trying to remove document that doesn't exist
        mutations.setDoc(state, { documentPath: "c1/s3" });
        expect(log.error).toHaveBeenCalledTimes(3);
        expect(state).toEqual(state2);
    });

    //setBusy
    test("setBusy: is setting the flag", () => {
        const state: any = { _registeredPaths: {}, isBusy: false };
        mutations.setBusy(state);
        expect(state.isBusy).toEqual(true);
        mutations.setBusy(state, false);
        expect(state.isBusy).toEqual(false);
    });
    test("setBusy: nested calls are working okay", () => {
        const state2: any = { _registeredPaths: {}, isBusy: false };
        mutations.setBusy(state2);
        mutations.setBusy(state2);
        mutations.setBusy(state2, false);
        expect(state2.isBusy).toEqual(true);
        mutations.setBusy(state2, false);
        expect(state2.isBusy).toEqual(false);
    });

    //addRegisteredPath
    test("addRegisteredPath: adds path to _registeredPaths", () => {
        const state: any = { _registeredPaths: {} };
        mutations.addRegisteredPath(state, "c1/d1/c11");
        expect(state._registeredPaths["c1/d1/c11"]).not.toBeUndefined();
    });
    test("addRegisteredPath: can handle invalid path", () => {
        const state: any = { _registeredPaths: {} };
        const state2 = JSON.parse(JSON.stringify(state));
        mutations.addRegisteredPath(state, "");
        expect(log.error).toHaveBeenCalled();
        expect(state).toEqual(state2);
    });

    //removeRegisteredPath
    test("removeRegisteredPath: removes path from _registeredPaths and cleans data from state", () => {
        const state: any = { _registeredPaths: { "c1/d1/c2/d2": true }, c1: { d1: { c2: { d2: { a: 1 } } } } };
        mutations.removeRegisteredPath(state, "c1/d1/c2/d2");
        expect(state._registeredPaths["c1/d1/c2/d2"]).toBeUndefined();
        expect(state.c1.d1.c2).toBeUndefined();
    });
    test("removeRegisteredPath: can handle invalid path", () => {
        const state: any = { _registeredPaths: { "c1/d1": true } };
        mutations.removeRegisteredPath(state, "");
        mutations.removeRegisteredPath(state, "");
        expect(log.error).toHaveBeenCalled();
        expect(state._registeredPaths["c1/d1"]).not.toBeUndefined();
    });
    test("removeRegisteredPath: handle deeper mapping paths", () => {
        const state: any = {
            _registeredPaths: {
                "cv": true,
                "cv/uid": true,
                "cv/uid/personalDetail": true,
            },
            cv: {
                uid: {
                    personalDetail: {
                        doc1: {},
                        doc2: {}
                    },
                    uid: "uid"
                }
            }
        };
        mutations.removeRegisteredPath(state, "cv/uid");
        expect(state).toHaveProperty("cv.uid.personalDetail");
        expect(state).not.toHaveProperty("cv.uid.uid");
        mutations.removeRegisteredPath(state, "cv");
        expect(state).toHaveProperty("cv.uid.personalDetail");
    });
});
