import Vue from "vue/dist/vue.common";
import Vuex from "vuex";
import firebase from "firebase";
import firedb from "../../src/firedb";

class PersonalDetails {
  fullName?: string;
  address?: string;
  constructor(obj: PersonalDetails) {
    this.fullName = obj && obj.fullName ? obj.fullName : undefined;
    this.address = obj && obj.address ? obj.address : undefined;
  }
}

Vue.use(Vuex);
const store = new Vuex.Store({
  state: {},
  plugins: [firedb]
});

(window as any).vue = new Vue({
  el: '#app',
  store: store,
  data: {
    firebaseConfig: "",
    firebaseInitialised: false,
    registerPath: "pop",
    unregisterPath: "pop",
    docPath: "pop",
    document: "{\"a\":\"1\"}",
  },
  computed: {
    fdb() {
      return JSON.stringify(this.$store.state.firedb, null, 2);
    }
  },
  methods: {
    initialiseFirebase() {
      if (this.firebaseConfig.trim()) {
        firebase.initializeApp(JSON.parse(this.firebaseConfig));
        firebase.auth().signInWithPopup(new firebase.auth.GoogleAuthProvider());
        const settings = {/* your settings... */ timestampsInSnapshots: true };
        firebase.firestore().settings(settings);
        this.firebaseInitialised = true;
      }
    },
    register() {
      this.$store.dispatch("firedb/registerPath", this.registerPath);
    },
    unregister() {
      this.$store.dispatch("firedb/unregisterPath", this.unregisterPath);
    },
    setDoc() {
      if (this.document.trim()) {
        this.$store.dispatch("firedb/set", { path: this.docPath, value: JSON.parse(this.document) })
      } else {
        this.$store.dispatch("firedb/set", { path: this.docPath, value: null })
      }
    }
  }
});

(window as any).firebase = firebase;
